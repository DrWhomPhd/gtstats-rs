pub mod database;
pub mod timing;

use clap::{Parser, ValueEnum};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum Timers {
    TSLV1,
    TSLV2,
    WEC,
    MULTILOOP,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum Databases {
    POSTGRES,
    SQLITE,
}
#[derive(Parser, Debug)]
#[command(name = "GTStats")]
#[command(version = "0.1")]
#[command(about = "Ingests, digests, and stores endurace race timing data.")]
struct CLI_Arguments {
    #[arg(short,long)]
    timer_type: Timers,

    #[arg(short,long)]
    db_type: Databases,

    #[arg(short,long, required_if_eq("db_type", "POSTGRES"), help("Postgres Connection URI. Required when using postgres."))]
    connection_str: Option<String>,

    #[arg(short,long, required_if_eq("db_type", "SQLITE"), help("Required when using sqlite."))]
    filename: Option<String>
}

fn main() {
    let args = CLI_Arguments::parse();

    dbg!(args);
}
