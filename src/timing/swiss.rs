use std::collections::HashMap;

/// Swiss Timing Protocol Format
/// <TODO: Get the necessary information to connect to the web socket>
/// |Header Length: 10 Bytes||Header: JSON Format||Body -- rest of received socket.|
/// 
/// Header Fields:
/// compressor: [lzw]
/// format: [json]
/// type: [data]
/// 
/// Body Fields:
/// <TODO>

fn lzw_decode(mut byte_string: &[u8] ) -> String {
    // Build the dictionary.
    let mut dictionary: HashMap::<u32, Vec<u8>> = (0u32..=255)
        .map(|i| (i, vec![i as u8]))
        .collect();

    let mut w = dictionary[&byte_string[0].into()].clone();
    byte_string = &byte_string[1..];
    let mut decompressed = w.clone();

    for &k in byte_string {
        let entry = if dictionary.contains_key(&k.into()) {
            dictionary[&k.into()].clone()
        } else if k == dictionary.len() as u8 {
            let mut entry = w.clone();
            entry.push(w[0]);
            entry
        } else {
            panic!("Invalid dictionary!");
        };

        decompressed.extend_from_slice(&entry);

        // New sequence; add it to the dictionary.
        w.push(entry[0]);
        dictionary.insert(dictionary.len() as u32, w);

        w = entry;
    }

    String::from_utf8(decompressed).expect("Could Not decode lzw bytes")

    // Original Javascript
    // static lzw_decode_internal(e) {
    //     for (var t = e.length, n = [], i = 0; i < t; i++) n.push(e[i].charCodeAt(0));
    //     for (var o, r, s, a = [], l = "", u = 256, i = 0; i < 256; i += 1) a[i] = String.fromCharCode(i);
    //     for (r = o = String.fromCharCode(n[0]), i = 1; i < n.length; i += 1) r += l = a[s = n[i]] ? a[s] : s === u ? o + o.charAt(0) : String.fromCharCode(s), a[u++] = o + l.charAt(0), o = l;
    //     return r
    // }
    
}

#[cfg(test)]
mod tests {
    use super::lzw_decode;

    #[test]
    fn test_lzw_decode() {
        let out = lzw_decode("%7B%22codeÄ�2%3AÄ�dataÄ�Ä�CÄ�ChannelÄ�Ä�Ä�RAC_PRODÄ�CSÄ§_2024_TIMING_00C45964-B3E1-Ä¾BB-8Å�Å�EEDÅ�C3BE01E_JSONÄ�2Ä�Ä�syncÄ�A36Ä±Ä�Ä�pushDÄ�Ä�Ä�Ä�Ä�5Ä�5Ä©7D".as_bytes());
        dbg!(out);
    }

    fn test_swiss_msg_decode() {
        todo!()
    }

}