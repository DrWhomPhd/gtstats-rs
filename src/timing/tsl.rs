/* Target Types
    CompetitorIntermediate,
    RCMsgReceived,
    ResultUpdated,
    CompetitorPitOut,
    CompetitorLapCompleted,
    SessionUpdated
}
*/
/* Test JSON 

send: 
{"protocol":"json","version":1}
{"arguments":["242704"],"target":"registerForEvent","type":1}
{"type":6} -- every 5 seconds after first received {}
receive:
{"type":1,"target":"CompetitorIntermediate","arguments":[{"id":"24a78921-1e1b-9084-e415-1074a07413e6","sectorKey":"S1Time","time":"52.254","timePB":false,"speedTrapKey":"Int1Speed","speed":29.211295034079846,"speedPB":false}]}
{"type":1,"target":"CompetitorIntermediate","arguments":[{"id":"7eef8f43-e0b8-729c-987d-71c1cce7b80a","sectorKey":"S2Time","time":"56.150","timePB":false,"speedTrapKey":"Int2Speed","speed":37.174721189591075,"speedPB":false}]}
{"type":1,"target":"CompetitorIntermediate","arguments":[{"id":"d37e8963-5d9e-d386-066b-f7e579d66f69","sectorKey":"S1Time","time":"41.628","timePB":false,"speedTrapKey":"Int1Speed","speed":38.41229193341869,"speedPB":false}]}
{"type":1,"target":"RCMsgReceived","arguments":[{"timestamp":"2024-07-05T18:28:04.1955871+01:00","lineNo":1,"text":"","urgent":false}]}
{"type":1,"target":"ResultUpdated","arguments":[{"id":"d37e8963-5d9e-d386-066b-f7e579d66f69","no":"84","name":"LAIDLOW / LAIDLOW","team":null,"primaryClass":"SIDE","subClass":"CUP","vehicle":"LCR Yamaha","sponsor":"Express Tyre Service / Pendragon Services Inc / Eife Racing / Marin Motorsport","nationality":"","manufacturer":"Yamaha","result":{"position":3,"pic":1,"posChange":0,"laps":8,"raceTime":"21:07.937","fastLapTime":"2:01.007","gap":"1.908","diff":"1.723","pitStops":3},"lastLapTime":"2:01.007","state":1}]}
{"type":1,"target":"ResultUpdated","arguments":[{"id":"424cf17b-2d6e-01d9-5722-620aa2b7ac27","no":"93","name":"G.HOLDEN / LAWRENCE","team":null,"primaryClass":"SIDE","subClass":"GP","vehicle":"LCR Kawasaki","sponsor":"Holden Racing","nationality":"","manufacturer":"Kawasaki","result":{"position":4,"pic":3,"posChange":0,"laps":9,"raceTime":"19:00.732","fastLapTime":"2:01.609","gap":"2.510","diff":"0.602","pitStops":1},"lastLapTime":"2:01.609","state":4}]}
{"type":1,"target":"CompetitorPitOut","arguments":[{"id":"baf3739e-ee77-6820-dc3f-4e9ed5943265"}]}
{"type":1,"target":"CompetitorLapCompleted","arguments":[{"id":"2429393c-90d0-2f1c-9380-08e7e771286b","lap":7,"time":"7:07.927"}]}
{"type":1,"target":"SessionUpdated","arguments":[{"sessionFlag":"Finish","sessionClock":{"timeToGo":"00:00:00","running":false},"id":"a6578460-c01f-1982-7d21-7f65c5d62a09","series":"2024 Life Safety Systems British Sidecar Championship","name":"QUALIFYING","type":2,"plannedStart":"2024-07-05T18:05:00+01:00","weatherConditions":"Cloudy","trackConditions":"Dry","units":1,"duration":{"time":"00:20:00","laps":0},"fastestLap":{"id":"b99deda7-293d-68a2-755c-3102d7895850","lapTime":"1:59.099"},"track":{"name":"Snetterton 300 - 2 Int","displayName":"Snetterton 300","length":4778,"sectors":[{"name":"Sector 1","key":"S1Time","start":0,"length":1545,"isSpeedTrap":false,"end":1545,"sessionBest":{"id":"79032070-66a9-bd54-9133-c4260d6ce74e","time":"37.441"}},{"name":"Int 1 Speed","key":"Int1Speed","start":1515,"length":30,"isSpeedTrap":true,"end":1545,"sessionBest":{"id":"79032070-66a9-bd54-9133-c4260d6ce74e","speed":54.24954792043399}},{"name":"Sector 2","key":"S2Time","start":1545,"length":1653,"isSpeedTrap":false,"end":3198,"sessionBest":{"id":"b99deda7-293d-68a2-755c-3102d7895850","time":"42.175"}},{"name":"Int 2 Speed","key":"Int2Speed","start":3168,"length":30,"isSpeedTrap":true,"end":3198,"sessionBest":{"id":"057f51dc-5a4e-aea4-d3f8-41a7d4a42c32","speed":61.7283950617284}},{"name":"Sector 3","key":"S3Time","start":3198,"length":1580,"isSpeedTrap":false,"end":4778,"sessionBest":{"id":"b99deda7-293d-68a2-755c-3102d7895850","time":"39.279"}},{"name":"Finish Speed","key":"FinSpeed","start":4748,"length":30,"isSpeedTrap":true,"end":4778,"sessionBest":{"id":"424cf17b-2d6e-01d9-5722-620aa2b7ac27","speed":57.915057915057915}}]},"classes":[{"id":"d0c78204-2f98-e13e-a0f2-3fc8b5c38a2a","name":"SIDE","colour":"#00000000"},{"id":"a7ccbee3-c48f-921b-c1ca-ffe4b9414ac1","name":"SIDE - CUP","colour":"#FFFFFF00"},{"id":"1856910c-d32b-6cbf-f129-aba04ef661c1","name":"SIDE - F2","colour":"#FFFF00FF"},{"id":"ae7d75b7-7369-f28d-4113-d0f324c0a3a4","name":"SIDE - GP","colour":"#FF00FFFF"},{"id":"097051e9-9fe0-ee38-c294-81940b266459","name":"SIDE - WC","colour":"#FFC0C0C0"}]}]}

*/
