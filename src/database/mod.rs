pub mod postgres;
pub mod sqlite;

use std::collections::HashMap;
use serde::{Deserialize, Serialize};

// LapInfo is the top level structure that will be serialized and inserted into the database
#[derive(Serialize, Deserialize, Debug)]
pub struct RaceInfo {
    laps: HashMap<usize, Lap>, // <lap number, lap info>
    pit_count: u16,
    fastet_lap: f32,
}

impl RaceInfo {
    fn to_json(&self) -> String {
        serde_json::to_string(self).expect("Could not serialize RaceInfo.")
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Lap {
    driver: String, // Driver of the current lap
    gap: f32, // Gap to car in front
    last_lap: f32, // Last lap time
    s1: f32, // Sector 1 time
    s2: f32, // Sector 2 time
    s3: f32, // Sector 3 time
    notes: String, // Misc information regarding the lap
}

// Table creation SQL.
const TABLE_SCHEMA: &str = "
    CREATE TABLE IF NOT EXISTS race_table (
        race_name text,
        car_number int,
        race_info jsonb
    );
";

// Create Unique Index Query unique_entry_idx.
// Creates a unix index pairing race_name and car_number to use with ON CONFLICT 
const UNIQUE_INDEX: &str = "
    CREATE UNIQUE INDEX unique_entry_idx ON
        race_table (race_name, car_number);
";

// Inserts car information for the race or will update the race info JSON
// if the are has already been added. The SQLite and PostgreSQL syntax
// is the same.
const UPSERT_RACEINFO: &str = "
    INSERT INTO race_table (race_name, car_number, race_info)
        VALUES ($1, $2, $3)
        ON CONFLICT (unique_entry_idx) DO UPDATE SET race_info = EXCLUDED.race_info;
";

pub trait GTStatsDB {
    #[allow(async_fn_in_trait)]
    /// Creates an object to track database connection state
    async fn new(uri: String) -> Self;
    #[allow(async_fn_in_trait)]
    /// Connect to an object and save state in struct
    async fn connect(&mut self);
    #[allow(async_fn_in_trait)]
    /// Upsert the race_name table for car_number with RaceInfo data
    async fn upsert(&mut self, car_number: u32, data: &RaceInfo);
    #[allow(async_fn_in_trait)]
    /// Add a new table to the database for race_name
    async fn new_race(&mut self, race_name: String);
}