use std::str::FromStr;

use super::GTStatsDB;
use sqlx::{postgres::{PgConnectOptions, PgPoolOptions}, PgPool};
pub struct PostgresBackend {
    connect_uri: String,
    connection: Option<sqlx::postgres::PgPool>,
    race_name: String,
}

impl GTStatsDB for PostgresBackend {
    async fn new(uri: String) -> Self {
        PostgresBackend {
            connect_uri: uri,
            connection: None,
            race_name: "".to_string(),
        }
    }
    async fn connect(&mut self) {
        let connect_options = PgConnectOptions::from_str(&self.connect_uri).unwrap().ssl_mode(sqlx::postgres::PgSslMode::Require);
        self.connection = Some(PgPoolOptions::new()
            .max_connections(10)
            .connect_with(connect_options).await.expect("Could not connect to Postgres DB"));

    }
    async fn upsert(&mut self, car_number: u32, data: &super::RaceInfo) {         
        let _ = sqlx::query::<sqlx::Postgres>(super::UPSERT_RACEINFO)
            .bind(&self.race_name)
            // Postgres backend does not support u32 thus sqlx thinks this is a SQLite Query
            .bind(i32::try_from(car_number).expect(format!("Car number greater than {}", i32::MAX).as_str()))
            .bind(data.to_json())
            .execute(self.connection.as_ref().unwrap()).await;
    }
    async fn new_race(&mut self, race_name: String) {
        self.race_name = race_name;
        let executor = self.connection.as_ref().unwrap();
        sqlx::query(super::TABLE_SCHEMA).execute(executor).await.expect("Error creating new race table");
        sqlx::query(super::UNIQUE_INDEX).execute(executor).await.expect("Error creating unique index.");
    }
}