use std::str::FromStr;

use super::GTStatsDB;
use sqlx::{sqlite::SqliteConnectOptions, ConnectOptions};
pub struct SqliteBackend {
    filename: String,
    connection: Option<sqlx::SqliteConnection>,
    race_name: String,
}

impl Default for SqliteBackend {
    fn default() -> Self {
        SqliteBackend {
            filename: String::from("sqlite::memory:"),
            connection: None,
            race_name: "".to_string(),
        }
    }
}

impl GTStatsDB for SqliteBackend {
    async fn new(uri: String) -> Self {
        SqliteBackend {
            filename: format!("sqlite://{}", uri),
            connection: None,
            race_name: "".to_string(),
        }
    }
    async fn connect(&mut self) {
        self.connection = Some(SqliteConnectOptions::from_str(&self.filename).expect("Could not create database configuration")
            .create_if_missing(true).connect().await.expect("Could not connect to SQLite Database"));
    }
    async fn upsert(&mut self, car_number: u32, data: &super::RaceInfo) {
        sqlx::query(super::UPSERT_RACEINFO)
        .bind(&self.race_name)
        .bind(car_number)
        .bind(data.to_json())
        .execute(self.connection.as_mut().unwrap()).await.expect("Error updating race data");
    }
    async fn new_race(&mut self, race_name: String) {
        self.race_name = race_name;
        sqlx::query(super::TABLE_SCHEMA).execute(self.connection.as_mut().unwrap()).await.expect("Error creating new race table");
        sqlx::query(super::UNIQUE_INDEX).execute(self.connection.as_mut().unwrap()).await.expect("Error creating unique index.");
    }
}

#[cfg(test)]
mod tests {
    use crate::database::GTStatsDB;

    use super::SqliteBackend;

    #[tokio::test]
    async fn test_sqlite_conect() {
        let mut db = SqliteBackend::default();
        db.connect().await;
    }

    #[tokio::test]
    async fn test_sqlite_newrace() {
        let mut db = SqliteBackend::default();
        db.connect().await;
        db.new_race(String::from("testrace")).await;
    }

    #[tokio::test]
    async fn test_sqlite_upsert() {
        let mut db = SqliteBackend::default();
        db.connect().await;
        db.new_race(String::from("testrace")).await;
    }
}