# GTStats Library

This library (and the CLI application that implements it) exists to ingest timing data for endurance racing, digest it, and output the data, reformatted, into a database. The back-end database can be leveraged by statistical languages such as R to create statistical analysis of race timing data. The 'utils' directory will store this statistical analysis data.

## Pre-Requisites
- [ ] Rust
- [ ] Postgres (and a DB) or Sqlite
- [ ] GoDaddy Certificate Bundles - G2 (https://certs.godaddy.com/repository) added to system certificate store (/etc/pki/ca-trus/source/anchors/ on Redhat Distros).

## Compilation

## Usage
```bash
Ingests, digests, and stores endurace race timing data.

Usage: gtstats-rs [OPTIONS] --timer-type <TIMER_TYPE> --db-type <DB_TYPE>

Options:
  -t, --timer-type <TIMER_TYPE>          [possible values: tslv1, tslv2, wec, multiloop]
  -d, --db-type <DB_TYPE>                [possible values: postgres, sqlite]
  -c, --connection-str <CONNECTION_STR>  Postgres Connection URI. Required when using postgres.
  -f, --filename <FILENAME>              Required when using sqlite.
  -h, --help                             Print help
  -V, --version                          Print version
```
